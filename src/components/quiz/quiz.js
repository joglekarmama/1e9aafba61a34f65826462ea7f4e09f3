import React, { useState, useEffect, useContext, Fragment } from 'react';
import Styled from 'styled-components';
import config from 'visual-config-exposer';

import QuizCards from '../quizCards/quizCards';
import { GameContext } from '../../context/gameContext';
import { ScoreContext } from '../../context/scoreContext';
import './quiz.css';

const Container = Styled.div`
background-image: url(${config.quizSettings.quizBackground});
background-size: cover;
background-repeat: no-repeat;
background-position: center;
`;

let startTime, endTime, time, interval;

const Quiz = () => {
  const gameContext = useContext(GameContext);
  const scoreContext = useContext(ScoreContext);
  const [timeUp, setTimeUp] = useState(false);
  const [completed, setCompleted] = useState(false);

  useEffect(() => {
    startTime = new Date();
    interval = setInterval(quizEndHandler, 1000);
  }, []);

  const quizEndHandler = (status) => {
    endTime = new Date();
    time = (endTime - startTime) / 1000;
    console.log(status);
    if (status === 'COMPLETED') {
      setCompleted(true);
      clearInterval(interval);
      setTimeout(() => {
        gameContext.setPostScreen();
      }, 3000);
    } else if (time >= config.quizSettings.quizTime) {
      clearInterval(interval);
      setTimeout(() => {
        gameContext.setPostScreen();
      }, 3000);
      setTimeUp(true);
    }
  };

  return (
    <Fragment>
      {!completed && !timeUp ? (
        <Fragment>
          {config.quizSettings.showScore && (
            <div className="score">Score : {scoreContext.score}</div>
          )}
          <Container className="quiz__container">
            <QuizCards quizEnd={quizEndHandler} />
          </Container>
        </Fragment>
      ) : completed ? (
        <Container className="quiz__container">
          <h1>Quiz Completed</h1>
          <h1>Score : {scoreContext.score}</h1>
        </Container>
      ) : (
        <Container className="quiz__container">
          <h1>Time Up </h1>
          <h1>Score : {scoreContext.score}</h1>
        </Container>
      )}
    </Fragment>
  );
};

export default Quiz;

// {!timeUp ? <h1>Quiz Screen</h1> : <h1>Time Up</h1>}
