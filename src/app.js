import React, { useContext } from 'react';
import Styled from 'styled-components';
import config from 'visual-config-exposer';

import { GameContext } from './context/gameContext';
import PreQuiz from './components/preQuiz/preQuiz';
import PostQuiz from './components/postQuiz/postQuiz';
import Quiz from './components/quiz/quiz';
import './app.css';

const Main = Styled.main`
background-image: url(${config.preQuizScreen.bgImg});
background-size: cover;
background-repeat: no-repeat;
background-position: center;
`;

const App = () => {
  const gameContext = useContext(GameContext);

  return (
    <Main className="main">
      {gameContext.screen === 'PRE' && <PreQuiz />}
      {gameContext.screen === 'MAIN' && <Quiz />}
      {gameContext.screen === 'POST' && <PostQuiz />}
    </Main>
  );
};

export default App;
